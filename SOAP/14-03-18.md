#Scripture

How can a young person stay on the path of purity? By living according to your word. 
I seek you with all my heart; do not let me stray from your commands.
I have hidden your word in my heart that I might not sin against you.

Psalm 119:9‭-‬11 NIV


#Observation

God's Word is the only instructions for purity. By being away from His Word, we open ourselves up to impurity and sin. When we only seek God's commandments, we cannot desire sin. It's good to memorize Scripture, but we should meditate on it until it's hidden in our hearts. 


#Application. 

I must meditate on God's Word day and night. Scripture should be my first response to any issue. Continue with these daily SOAPs to have a deeper understanding of God. 


#Prayer

Lord, thank you for giving me your Word. I'm so blessed and grateful to live in a society that doesn't outlaw the Bible. I'm sorry for the many times I've taken it for granted, and have never really loved it as I should. Lord, give me a true desire for your Word. Speak to me in a way I've never heard before. Help me to stay away from sinful distractions that would take all my time. I want to prioritize my relationship with you above all. 